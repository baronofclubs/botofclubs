#################################################
##                   README                    ##
#################################################

BotOfClubs v0.3.1 - 2016-06-04

BotOfClubs is an IRC Bot based on the PircBotX 
framework. It is primarily built for Twitch.tv
at the moment, but grand plans are ahead.

It is completely incomplete. I make absolutely 
no promises or guarantees that this will work
for anything or anyone. There are no install
instructions yet, that'll come later. There's
no list of dependencies. There's no commented
code. You're on your own if you even want to 
look at it. You've been warned.