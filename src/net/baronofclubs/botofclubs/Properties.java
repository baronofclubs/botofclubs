package net.baronofclubs.botofclubs;

import java.time.ZonedDateTime;

// Properties Interface - Used to facilitate using subclasses.

import com.google.gson.JsonObject;

public interface Properties {
    void setToDefaults();
    JsonObject getAsJsonObject();
    void fromJsonObject(JsonObject jsonObject);
    
    boolean save();
    boolean read();
    boolean load();
    boolean delete();
    boolean copy();
    boolean exists();
    ZonedDateTime getLastSave();
}
