package net.baronofclubs.botofclubs.channel;

import java.time.ZonedDateTime;

import com.google.gson.JsonObject;

public class ChannelUser {

	private String name;
	private boolean banned;
	private ZonedDateTime firstJoined;
	private ZonedDateTime lastMessage;
	private int messageCount;
	private int channelPermission;

	public ChannelUser(String name) {
		this.name = name;
		this.banned = false;
		this.firstJoined = ZonedDateTime.now();
		this.lastMessage = ZonedDateTime.now();
		this.messageCount = 1;
		this.channelPermission = 1;
	}

	public String getName() {
		return name;
	}

	public boolean isBanned() {
		return banned;
	}

	public void setBanned(boolean banned) {
		this.banned = banned;
	}

	public ZonedDateTime getLastMessage() {
		return lastMessage;
	}

	public void setLastMessage(ZonedDateTime lastMessage) {
		this.lastMessage = lastMessage;
	}

	public int getChannelPermission() {
		return channelPermission;
	}

	public void setChannelPermission(int channelPermission) {
		this.channelPermission = channelPermission;
	}

	public ZonedDateTime getFirstJoined() {
		return firstJoined;
	}
	
	public int getMessageCount() {
		return messageCount;
	}

	public void setMessages(int messages) {
		this.messageCount = messages;
	}
	
	public void incrementMessages() {
		messageCount++;
	}
	
	public void chatted() {
		incrementMessages();
		lastMessage = ZonedDateTime.now();
	}
	
	public JsonObject getAsJsonObject() {
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("name", name);
		jsonObject.addProperty("banned", banned);
		jsonObject.addProperty("firstJoined", firstJoined.toString());
		jsonObject.addProperty("lastMessage", lastMessage.toString());
		jsonObject.addProperty("messageCount", messageCount);
		jsonObject.addProperty("channelPermission", channelPermission);
		
		return jsonObject;
	}
	
	public void fromJsonObject(JsonObject jsonObject) {
		name = jsonObject.get("name").getAsString();
		banned = jsonObject.get("banned").getAsBoolean();
		firstJoined = ZonedDateTime.parse(jsonObject.get("firstJoined").getAsString());
		lastMessage = ZonedDateTime.parse(jsonObject.get("lastMessage").getAsString());
		messageCount = jsonObject.get("messageCount").getAsInt();
		channelPermission = jsonObject.get("channelPermission").getAsInt();
	}
	
	
}
