package net.baronofclubs.botofclubs.channel;

import java.io.File;
import java.nio.file.Paths;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import net.baronofclubs.botofclubs.AutoSaver;
import net.baronofclubs.botofclubs.GlobalSettings;
import net.baronofclubs.botofclubs.Properties;
import net.baronofclubs.botofclubs.PropertiesFile;
import net.baronofclubs.botofclubs.command.CommandReference;
import net.baronofclubs.botofclubs.server.Server;

public class Channel {

	public ChannelProperties channelProperties;
	private Server server;

	public Channel(String name, Server server) {
		System.out.println(GlobalSettings.SDP + "Creating Channel - " + name);
		
		this.channelProperties = new ChannelProperties(name, server);
		this.server = server;
	}
	
	public void join() {
		server.joinChannel(this);
	}
	
	public void part() {
		server.partChannel(this);
	}
	
	public class ChannelProperties extends PropertiesFile implements Properties {

		private String name;
		private UUID channelKey;
		
		private String serverName;
		private ZonedDateTime firstJoined;
		private ZonedDateTime lastJoined;
		private char commandPrefix;
		private char adminPrefix;
		private int botLevel;
		private boolean autoJoin;
		private ZoneId timeZoneId;
		private UserList users;

		private CommandReference commandReference;
		
		
		public ChannelProperties(String name, Server server) {
			super(name + ".json", Paths.get("server" + File.separator + server.getName() + File.separator + "channels" + File.separator));
			new AutoSaver(this);
			this.name = name;
			this.serverName = server.getName();
			this.commandReference = new CommandReference();
			this.lastJoined = ZonedDateTime.now();
			this.users = new UserList();
			if(!this.load()) {
				this.create();
			}
			this.save();
		}
		
		@Override
		public void setToDefaults() {
			this.channelKey = createChannelKey();
			this.firstJoined = ZonedDateTime.now();
			this.lastJoined = ZonedDateTime.now();
			this.commandPrefix = GlobalSettings.defaultCommandPrefix;
			this.adminPrefix = GlobalSettings.defaultAdminPrefix;
			this.botLevel = 0;
			this.autoJoin = true;
			this.timeZoneId = GlobalSettings.timeZone.toZoneId();
		}

		public ZoneId getTimeZoneId() {
			return timeZoneId;
		}

		public void setTimeZoneId(ZoneId timeZoneId) {
			this.timeZoneId = timeZoneId;
			save();
		}

		private UUID createChannelKey() {
			return UUID.randomUUID();
		}

		@Override
		public JsonObject getAsJsonObject() {
			
			JsonObject jsonObject = new JsonObject();
			jsonObject.addProperty("name", this.name);
			jsonObject.addProperty("channelKey", this.channelKey.toString());
			jsonObject.addProperty("firstJoined", this.firstJoined.toString());
			jsonObject.addProperty("lastJoined", this.lastJoined.toString());
			jsonObject.addProperty("commandPrefix", this.commandPrefix);
			jsonObject.addProperty("adminPrefix", this.adminPrefix);
			jsonObject.addProperty("botLevel", this.botLevel);
			jsonObject.addProperty("autoJoin", this.autoJoin);
			jsonObject.add("commandReference", commandReference.getAsJsonObject());
			jsonObject.add("users", users.getAsJsonObject());
			jsonObject.addProperty("timeZoneId", timeZoneId.getId());

			return jsonObject;
		}

		@Override
		public void fromJsonObject(JsonObject jsonObject) {

			this.name = parseJsonMember(jsonObject, "name", "UNDEFINED"); // Parse String
			this.channelKey = parseJsonMember(jsonObject, "channelKey", UUID.randomUUID()); // Parse UUID
			this.firstJoined = parseJsonMember(jsonObject, "firstJoined", ZonedDateTime.now()); // Parse ZonedDateTime
			this.commandPrefix = parseJsonMember(jsonObject, "commandPrefix", GlobalSettings.defaultCommandPrefix); // Parse char
			this.adminPrefix = parseJsonMember(jsonObject, "adminPrefix", GlobalSettings.defaultAdminPrefix); // Parse char
			this.botLevel = parseJsonMember(jsonObject, "botLevel", 0); // Parse int
			this.autoJoin = parseJsonMember(jsonObject, "autoJoin", false); // Parse boolean
			this.timeZoneId = parseJsonMember(jsonObject, "timeZoneId", GlobalSettings.timeZone.toZoneId()); // Parse ZoneId
			
			this.commandReference.fromJsonObject(jsonObject.get("commandReference").getAsJsonObject()); // TODO: Custom - halts if null
			this.users = new UserList().fromJsonObject(jsonObject.get("users").getAsJsonObject()); // TODO: Custom - halts if null
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
			save();
		}


		public int getBotLevel() {
			return botLevel;
		}

		public void setBotLevel(int botLevel) {
			this.botLevel = botLevel;
			save();
		}

		public boolean isAutoJoin() {
			return autoJoin;
		}

		public void setAutoJoin(boolean autoJoin) {
			this.autoJoin = autoJoin;
			save();
		}

		public UUID getChannelKey() {
			return channelKey;
		}

		public String getServer() {
			return serverName;
		}

		public void setServer(String serverName) {
			this.serverName = serverName;
			save();
		}

		public void setLastJoined(ZonedDateTime lastJoined) {
			this.lastJoined = lastJoined;
			save();
		}

		public void setCommandPrefix(char commandPrefix) {
			this.commandPrefix = commandPrefix;
			save();
		}

		public void setAdminPrefix(char adminPrefix) {
			this.adminPrefix = adminPrefix;
			save();
		}

		public ZonedDateTime getFirstJoined() {
			return firstJoined;
		}

		public ZonedDateTime getLastJoined() {
			return lastJoined;
		}

		public char getCommandPrefix() {
			return commandPrefix;
		}

		public char getAdminPrefix() {
			return adminPrefix;
		}

		public String getChannelToken() {
			return "#" + name.toLowerCase(); // TODO: Change this so it gets the channel token from the server
		}

		public CommandReference getCommandReference() {
			return commandReference;
		}
		
		public ChannelUser getChannelUser(String name) {
			if (users.containsKey(name)) {
				return users.get(name);
			}
			addChannelUser(name);
			return users.get(name);
		}
		
		public void addChannelUser(String name) {
			users.put(name, new ChannelUser(name));
		}
		
		private class UserList extends HashMap<String, ChannelUser> {

			public JsonObject getAsJsonObject() {
				JsonObject jsonObject = new JsonObject();
				for (String name : this.keySet()) {
					ChannelUser channelUser = users.get(name);
					jsonObject.add(name, channelUser.getAsJsonObject());
				}
				return jsonObject;
			}
			
			public UserList fromJsonObject(JsonObject jsonObject) {
				for (Map.Entry<String,JsonElement> entry : jsonObject.entrySet()) {
					ChannelUser channelUser = new ChannelUser(entry.getKey());
					channelUser.fromJsonObject(entry.getValue().getAsJsonObject());
					this.put(entry.getKey(), channelUser);
				}
				return this;
			}
			
			
		}
	}

}
