package net.baronofclubs.botofclubs.channel;

import java.io.File;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.baronofclubs.botofclubs.server.Server;

public class ChannelReference {

	private Server server;
	private Map<String, Channel> channelReference = new HashMap<String, Channel>();
	
	public ChannelReference(Server server){
		this.server = server;
		//Channel defaultChannel = server.serverProperties.getDefaultChannel();
		//add(defaultChannel);
		
		loadAllFromDisk();
	}
	
	public Channel get(String name) {
		return channelReference.get(name);
	}
	
	public List<Channel> getAllAsList() {
		List<Channel> list = new ArrayList<Channel>();
		for (Channel channel : channelReference.values()) {
			list.add(channel);
		}
		return list;
	}

	public void add(Channel channel) {
		channelReference.put(channel.channelProperties.getName(), channel);
	}
	
	public void loadAllFromDisk() {
		File[] files = new File("server" + File.separator + server.getName() + File.separator + "channels" + File.separator).listFiles();

		for (File file : files) {
		    if (file.isFile()) {
		    	String channelName = file.getName().substring(0, file.getName().indexOf('.'));
		        Channel newChannel = new Channel(channelName, server);
		        add(newChannel);
		    }
		}
	}

	public boolean containsName(String name) {
		return channelReference.containsKey(name);
	}
}