package net.baronofclubs.botofclubs;

import com.google.gson.JsonObject;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.TimeZone;

public class GlobalSettings extends PropertiesFile implements Properties {

	public final transient static Path SETTINGS_PATH = Paths.get("settings/");
    public final transient static String SETTINGS_FILE = "settings.json";
    public final transient static String BOT_IDENTITY = "BotOfClubs";
    public final transient static String VERSION = "BotOfClubs 0.3.3";
    public final transient static Charset ENCODING = StandardCharsets.UTF_8;
    public final transient static String SDP = "BOTOFCLUBS: "; // System Debug Prefix
    
    public static int saveInterval;
    public static int loggingLevel;
    public static String defaultNick;
    public static TimeZone timeZone;
    public static char defaultCommandPrefix;
    public static char defaultAdminPrefix;
    
    public static Twitch twitch;
    
    public GlobalSettings() {
		super(SETTINGS_FILE, SETTINGS_PATH);
	}

    public void setToDefaults() {
        saveInterval = 60;
        loggingLevel = 0;
        defaultNick = "BotOfClubs";
        timeZone = TimeZone.getDefault();
        defaultAdminPrefix = '!';
        defaultCommandPrefix = '+';
        twitch = new Twitch();
    }

    @Override
    public JsonObject getAsJsonObject() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("saveInterval", Integer.toString(saveInterval));
        jsonObject.addProperty("loggingLevel", Integer.toString(loggingLevel));
        jsonObject.addProperty("defaultNick", defaultNick);
        jsonObject.addProperty("timeZone", timeZone.getID());
        jsonObject.addProperty("defaultCommandPrefix", defaultCommandPrefix);
        jsonObject.addProperty("defaultAdminPrefix", defaultAdminPrefix);

        return jsonObject;
    }

    @Override
    public void fromJsonObject(JsonObject jsonObject) {
        saveInterval = jsonObject.get("saveInterval").getAsInt();
        loggingLevel = jsonObject.get("loggingLevel").getAsInt();
        defaultNick = jsonObject.get("defaultNick").getAsString();
        timeZone = TimeZone.getTimeZone(jsonObject.get("timeZone").getAsString());
        defaultCommandPrefix = jsonObject.get("defaultCommandPrefix").getAsCharacter();
        defaultAdminPrefix = jsonObject.get("defaultAdminPrefix").getAsCharacter();

    }
    
    public static class Twitch extends PropertiesFile implements Properties {
    	
		public final transient static Path SETTINGS_PATH = Paths.get("settings/");
        public final transient static String SETTINGS_FILE = "Twitch.json";
        
    	public static String clientID;
    	private static String token;
    	
    	public Twitch() {
			super(SETTINGS_FILE, SETTINGS_PATH);
		}
    	
    	public void setToDefaults() {
    		clientID = "";
    		token = "";
    	}
    	
    	 @Override
    	 public JsonObject getAsJsonObject() {
    		 JsonObject jsonObject = new JsonObject();
    		 jsonObject.addProperty("client_id", clientID);
    		 jsonObject.addProperty("token", token);
    		 
    		 return jsonObject;
    	 }
    	 
	    @Override
	    public void fromJsonObject(JsonObject jsonObject) {
	    	clientID = jsonObject.get("client_id").getAsString();
	    	token = jsonObject.get("token").getAsString();
	    }

    	public static String getToken(){
    		// TODO: Add Token Generation with webserver.
    		return token;
    	}
    }

}
