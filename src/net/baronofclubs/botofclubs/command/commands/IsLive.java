package net.baronofclubs.botofclubs.command.commands;

import java.time.ZonedDateTime;

import net.baronofclubs.botofclubs.GlobalSettings;
import net.baronofclubs.botofclubs.apis.TwitchAPI;
import net.baronofclubs.botofclubs.command.Command;
import net.baronofclubs.botofclubs.command.CommandUtils;
import net.baronofclubs.botofclubs.server.Message;

public class IsLive extends CommandUtils implements Command {

	public IsLive() {
		// Set Variables
		this.enabled = true;
		this.name = "islive";
		this.aliases = new String[]{"islive"};
		this.lastRun = ZonedDateTime.parse("2016-07-04T12:00:00.000-07:00[America/Los_Angeles]");
		this.cooldown = 0;
		this.userLevel = 0;
		this.requiredArgs = 0;
	}
	
	@Override
	public String command(Message message) {
		TwitchAPI twitch = new TwitchAPI(GlobalSettings.Twitch.clientID);
		// TODO: Let these be variable and adjustable by channel owner.
		String channel = message.getChannel().channelProperties.getName();
		if (twitch.getStream(channel).isLive()) {
			return "I'm live! Currently playing " + twitch.getStream(channel).getGame();
		}
		return "Not currently streaming.";
	}

	@Override
	public String admin(Message message) {
		// TODO Auto-generated method stub
		return null;
	}

}
