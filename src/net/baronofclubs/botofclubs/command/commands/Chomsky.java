package net.baronofclubs.botofclubs.command.commands;

import java.time.ZonedDateTime;
import java.util.Arrays;

import org.apache.commons.lang3.StringUtils;

import net.baronofclubs.botofclubs.apis.Chomskybot;
import net.baronofclubs.botofclubs.command.Command;
import net.baronofclubs.botofclubs.command.CommandUtils;
import net.baronofclubs.botofclubs.server.Message;

public class Chomsky extends CommandUtils implements Command {
	
	public Chomsky() {
		// Set Variables
				this.enabled = true;
				this.name = "chomsky";
				this.aliases = new String[]{"chomsky", "peter"};
				this.lastRun = ZonedDateTime.parse("2016-07-04T12:00:00.000-07:00[America/Los_Angeles]");
				this.cooldown = 0;
				this.userLevel = 0;
				this.requiredArgs = 0;
	}
	
	// Primary Method
	@Override
	public String command(Message message) {
		// Here is where the code should lie.
		Chomskybot chomsky = new Chomskybot();
		String response = "";
		try {
			response = chomsky.botResponse("pandora", StringUtils.join(message.getArgs(), " "));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public String admin(Message message) {
		
		
		
		
		
		return null;
	}
	
	
}
