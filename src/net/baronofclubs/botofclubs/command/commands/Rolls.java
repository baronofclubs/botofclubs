package net.baronofclubs.botofclubs.command.commands;

import java.time.ZonedDateTime;
import java.util.Random;

import org.apache.commons.lang3.StringUtils;

import net.baronofclubs.botofclubs.command.Command;
import net.baronofclubs.botofclubs.command.CommandUtils;
import net.baronofclubs.botofclubs.server.Message;

public class Rolls extends CommandUtils implements Command {
	
	public Rolls(){
		// Variables Declaration
		this.enabled = true;
		this.name = "roll";
		this.aliases = new String[]{"roll", "rolls", "flips", "flip"};
		this.lastRun = ZonedDateTime.parse("2016-07-04T12:00:00.000-07:00[America/Los_Angeles]");
		this.cooldown = 30;
		this.userLevel = 0;
		this.requiredArgs = 0;
	}
	
	@Override
	public String command(Message message) {
		String[] args = message.getArgs();
		String user = message.getOriginUser().userProperties.getName();
		
		int theNumber = 0;
		String outString = "-1";
		String diedentifier = "d";
		int defaultRollMax = 20;
		
		if (args != null) {
			if (isInteger(args[0])) {
				theNumber = randInt(1, Integer.parseInt(args[0]));
				outString = user + " rolls 1d" + args[0] + " and it falls on " + Integer.toString(theNumber) + "!";
			} else if (args[0].contains(diedentifier)) {
				String[] dSep = args[0].split(diedentifier);
				if (isInteger(dSep[0]) && isInteger(dSep[1])) {
					if (Integer.parseInt(dSep[0]) > 1) {
						String multiRoll = Integer.toString(randInt(1, Integer.parseInt(dSep[1])));
						for (int i = 0; i < Integer.parseInt(dSep[0]) - 1; i++) {
							multiRoll = multiRoll + ", " + Integer.toString(randInt(1, Integer.parseInt(dSep[1])));
							outString = user + " rolls " + dSep[0] + " die. Results: " + multiRoll + ".";
						}
					} else {
						outString = user + " rolls a " + Integer.toString(randInt(1, Integer.parseInt(dSep[1]))) + "!";
					}
				} else {
					outString = user + " rolls " + randInt(1, defaultRollMax) + " " + StringUtils.join(args);
				}
			} else if (args[0].contains("coin")) {
				int coin = randInt(1, 2);
				if (coin == 1) {
					outString = user + " flips a coin that comes up heads";
				} else {
					outString = user + " flips a coin that comes up tails";
				}
			} else {
				outString = user + " rolls " + randInt(1, defaultRollMax) + " " + StringUtils.join(args, " ");
			}
		} else {
			int roller = randInt(1, defaultRollMax);
			if (roller == defaultRollMax) {
				outString = user + " got a critical by rolling " + Integer.toString(roller) + " on a 1d" + Integer.toString(defaultRollMax) + "!";
			} else if (roller == 1) {
				outString = user + " critically failed by rolling " + Integer.toString(roller) + " on a 1d" + Integer.toString(defaultRollMax) + "!";
			} else {
				outString = user + " rolled " + Integer.toString(roller) + " on a 1d" + Integer.toString(defaultRollMax) + "!";
			}
		}
		
		
	    return outString;
	}
	
	// Class Methods
	public static int randInt(int min, int max) {

	    Random rand = new Random();
	    int randomNum = rand.nextInt((max - min) + 1) + min;

	    return randomNum;
	}
	
	public static boolean isInteger(String str) {
	    if (str == null) {
	        return false;
	    }
	    int length = str.length();
	    if (length == 0) {
	        return false;
	    }
	    int i = 0;
	    if (str.charAt(0) == '-') {
	        if (length == 1) {
	            return false;
	        }
	        i = 1;
	    }
	    for (; i < length; i++) {
	        char c = str.charAt(i);
	        if (c < '0' || c > '9') {
	            return false;
	        }
	    }
	    return true;
	}

	@Override
	public String admin(Message message) {
		// TODO Auto-generated method stub
		return null;
	}
}
