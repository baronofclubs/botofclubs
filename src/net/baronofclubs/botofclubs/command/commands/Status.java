package net.baronofclubs.botofclubs.command.commands;

import java.time.ZonedDateTime;

import net.baronofclubs.botofclubs.GlobalSettings;
import net.baronofclubs.botofclubs.apis.TwitchAPI;
import net.baronofclubs.botofclubs.command.Command;
import net.baronofclubs.botofclubs.command.CommandUtils;
import net.baronofclubs.botofclubs.server.Message;

public class Status extends CommandUtils implements Command {

	public Status() {
		// Variables Declaration
		this.enabled = true;
		this.name = "status";
		this.aliases = new String[]{"status"};
		this.lastRun = ZonedDateTime.parse("2016-07-04T12:00:00.000-07:00[America/Los_Angeles]");
		this.cooldown = 0;
		this.userLevel = 0;
		this.requiredArgs = 0;
	}
	
	@Override
	public String command(Message message) {			
		
		String channelName = message.getChannel().channelProperties.getName();
		
		TwitchAPI twitch;
		if (message.getArgs() == null) {
			twitch = new TwitchAPI(GlobalSettings.Twitch.clientID, GlobalSettings.Twitch.getToken());
		} else {
			twitch = new TwitchAPI(GlobalSettings.Twitch.clientID);
		}
		 
		
		return twitch.getChannel(channelName).getStatus();
	}

	@Override
	public String admin(Message message) {
		// TODO Auto-generated method stub
		return null;
	}
}
