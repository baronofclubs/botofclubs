package net.baronofclubs.botofclubs.command.commands;

import java.time.ZonedDateTime;

import net.baronofclubs.botofclubs.GlobalSettings;
import net.baronofclubs.botofclubs.apis.TwitchAPI;
import net.baronofclubs.botofclubs.command.Command;
import net.baronofclubs.botofclubs.command.CommandUtils;
import net.baronofclubs.botofclubs.server.Message;

public class Viewers extends CommandUtils implements Command {

	public Viewers() {
		// Set Variables
		this.enabled = true;
		this.name = "viewers";
		this.aliases = new String[]{"viewers"};
		this.lastRun = ZonedDateTime.parse("2016-07-04T12:00:00.000-07:00[America/Los_Angeles]");
		this.cooldown = 0;
		this.userLevel = 0;
		this.requiredArgs = 0;
	}
	
	@Override
	public String command(Message message) {
		TwitchAPI twitchAPI = new TwitchAPI(GlobalSettings.Twitch.clientID);
		String channelName = message.getChannel().channelProperties.getName();
		int viewers = (int) twitchAPI.getStream(channelName).getViewers();
		if (viewers > 1) {
			return String.valueOf(viewers) + " viewers";
		} else if (viewers == 1) {
			return "One lonely viewer.";
		}
		return "No viewers right now, please try again later.";
		
	}

	@Override
	public String admin(Message message) {
		// TODO Auto-generated method stub
		return null;
	}

}