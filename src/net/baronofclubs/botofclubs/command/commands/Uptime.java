package net.baronofclubs.botofclubs.command.commands;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import org.apache.commons.lang3.time.DurationFormatUtils;

import net.baronofclubs.botofclubs.GlobalSettings;
import net.baronofclubs.botofclubs.apis.TwitchAPI;
import net.baronofclubs.botofclubs.channel.Channel;
import net.baronofclubs.botofclubs.command.Command;
import net.baronofclubs.botofclubs.command.CommandUtils;
import net.baronofclubs.botofclubs.server.Message;

public class Uptime extends CommandUtils implements Command {

	public Uptime() {
		// Set Variables
		this.enabled = true;
		this.name = "uptime";
		this.aliases = new String[]{"uptime"};
		this.lastRun = ZonedDateTime.parse("2016-07-04T12:00:00.000-07:00[America/Los_Angeles]");
		this.cooldown = 0;
		this.userLevel = 0;
		this.requiredArgs = 0;
	}
	
	@Override
	public String command(Message message) {
		TwitchAPI twitchAPI = new TwitchAPI(GlobalSettings.Twitch.clientID);
		String channelName = message.getChannel().channelProperties.getName();
		Channel channel = message.getChannel();
		ZonedDateTime updatedAt = twitchAPI.getStream(channelName).getCreatedAt();
		if (updatedAt == null) {
			return "Not currently streaming.";
		}
		ZonedDateTime channelTimeCreatedAt = updatedAt.withZoneSameInstant(channel.channelProperties.getTimeZoneId());
		
		long difference = ChronoUnit.SECONDS.between(updatedAt, ZonedDateTime.now());
		
		DateTimeFormatter dateTimeFormat;
		
		if (difference > 1440) {
			dateTimeFormat = DateTimeFormatter.ofPattern("h:mm ' 'a' on 'E', 'd MMMM', 'u' ('z')'");
		} else {
			dateTimeFormat = DateTimeFormatter.ofPattern("h:mm ' 'a' ('z')'");
		}
				
		return "Stream went live at " + channelTimeCreatedAt.format(dateTimeFormat)+ " - " + TimeIntervalFormatter(difference * 1000) + " ago.";
	}
	
	private String TimeIntervalFormatter(long timeInMillis) {
		long timeInSeconds = timeInMillis / 1000;
		long timeInMinutes = timeInSeconds / 60;
		long timeInHours = timeInMinutes / 60;
		long timeInDays = timeInHours / 24;
		long timeInYears = timeInDays / 365;
		
		if (timeInYears > 1) {
			return String.valueOf(timeInYears) + " years";
		} else if (timeInYears == 1) {
			return String.valueOf(timeInYears) + " year, " + String.valueOf(timeInDays - (timeInYears * 365)) + " days";
		} else if (timeInDays < 365 && timeInDays > 1) {
			return String.valueOf(timeInDays) + " days";
		} else if (timeInDays == 1) {
			return String.valueOf(timeInDays) + " day, " + String.valueOf(timeInHours - (timeInDays*24)) + " hours";
		} else if (timeInHours < 24 && timeInHours > 1) {
			return String.valueOf(timeInHours) + " hours, " + String.valueOf(timeInMinutes - (timeInHours * 60)) + " minutes";
		} else if (timeInHours == 1) {
			return String.valueOf(timeInHours) + " hour, " + String.valueOf(timeInMinutes - (timeInHours * 60)) + " minutes";
		} else if (timeInMinutes < 60 && timeInMinutes > 1) {
			return String.valueOf(timeInMinutes) + " minutes, " + String.valueOf(timeInSeconds - (timeInMinutes * 60)) + " seconds";
		} else if (timeInMinutes == 1) {
			return String.valueOf(timeInMinutes) + " minute, " + String.valueOf(timeInSeconds - (timeInMinutes * 60)) + " seconds";
		} else if (timeInSeconds < 60 && timeInSeconds > 1) {
			return String.valueOf(timeInSeconds) + " seconds";
		} else if (timeInSeconds == 1) {
			return "Just a second";
		} else {
			return "Less than a second";
		}
	}

	@Override
	public String admin(Message message) {
		// TODO Auto-generated method stub
		return null;
	}

}
