package net.baronofclubs.botofclubs.command.commands;

import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import net.baronofclubs.botofclubs.apis.StringSimilarity;
import net.baronofclubs.botofclubs.command.Command;
import net.baronofclubs.botofclubs.command.CommandUtils;
import net.baronofclubs.botofclubs.server.Message;

public class Time extends CommandUtils implements Command {
	
	public Time() {
		// Set Variables
		this.enabled = true;
		this.name = "servertime";
		this.aliases = new String[]{"servertime", "time"};
		this.lastRun = ZonedDateTime.parse("2016-07-04T12:00:00.000-07:00[America/Los_Angeles]");
		this.cooldown = 0;
		this.userLevel = 0;
		this.requiredArgs = 0;
	}
	
	// Primary Method
	@Override
	public String command(Message message) {
		// Here is where the code should lie.
		String[] args = message.getArgs();
		if (!(args == null)) {
			switch (args[0].toLowerCase()) {
			case "timezone": // TODO: Change this to admin command stuff
				if (args[1].equalsIgnoreCase("set") && args[2] != null) {
					ZoneId newZoneId = timeZoneParser(args[2]);
					message.getChannel().channelProperties.setTimeZoneId(newZoneId);
					return "Time Zone set to: " + newZoneId.getDisplayName(TextStyle.FULL, Locale.getDefault());
				}
				return "Time Zone currently set to: " + message.getChannel().channelProperties.getTimeZoneId().getDisplayName(TextStyle.SHORT, Locale.getDefault());
			}
		}
		
		DateTimeFormatter dateTimeFormat = DateTimeFormatter.ofPattern("h:mm ' 'a' on 'E', 'd MMMM', 'u' ('z')'");
				
    	return "The  time is now: " + ZonedDateTime.now(message.getChannel().channelProperties.getTimeZoneId()).format(dateTimeFormat);
	}
	
	@Override
	public String admin(Message message) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public ZoneId timeZoneParser(String stringToParse) {
		String upperStringToParse = stringToParse.toUpperCase();
		if (upperStringToParse.startsWith("GMT")) {
			if (StringUtils.isNumeric(upperStringToParse.substring(4))) {
				return ZoneId.ofOffset("GMT", ZoneOffset.ofHours(Integer.parseInt(upperStringToParse.substring(4))));
			}
		}
		if (upperStringToParse.startsWith("UTC")) {
			if (StringUtils.isNumeric(upperStringToParse.substring(4))) {
				return ZoneId.ofOffset("UTC", ZoneOffset.ofHours(Integer.parseInt(upperStringToParse.substring(4))));
			}
		}
		if (upperStringToParse.startsWith("UT")) {
			if (StringUtils.isNumeric(upperStringToParse.substring(3))) {
				return ZoneId.ofOffset("UT", ZoneOffset.ofHours(Integer.parseInt(upperStringToParse.substring(3))));
			}
		}
		
		Map<Double, String> simMap = new HashMap<Double, String>();
		for (String zoneId : ZoneId.getAvailableZoneIds()) {
			if (zoneId.toString().toLowerCase().contains(stringToParse.toLowerCase())){
				return ZoneId.of(zoneId);
			}
			simMap.put(StringSimilarity.similarity(zoneId, stringToParse), zoneId);
		}
		return ZoneId.of(simMap.get(Collections.max(simMap.keySet())));
	}
}
