package net.baronofclubs.botofclubs.command;

import java.time.ZonedDateTime;

import com.google.gson.JsonObject;

import net.baronofclubs.botofclubs.server.Message;

public interface Command {

	public boolean enabled();
	public void enable();
	public void disable();
	
	public String run(Message message);
	public String command(Message message);
	public String admin(Message message);
	
	public String getName();
	public String[] getAliases();
	public void addAlias(String newAlias);
	public void removeAlias(String alias);
	
	public ZonedDateTime getLastRun();
	public void setLastRun(ZonedDateTime run);
	public int getCooldown();
	public void setCooldown(int cdset);
	
	public int getUserLevel();
	public void setUserLevel(int ulset);
	
	public boolean isCool();

	public JsonObject getAsJsonObject();
	public void fromJsonObject(JsonObject jsonObject);
	
	public void setProperty(String property, String newValue);
	
}
