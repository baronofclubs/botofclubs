package net.baronofclubs.botofclubs.command;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import net.baronofclubs.botofclubs.command.commands.Chomsky;
import net.baronofclubs.botofclubs.command.commands.CustomCommand;
import net.baronofclubs.botofclubs.command.commands.IsLive;
import net.baronofclubs.botofclubs.command.commands.Rolls;
import net.baronofclubs.botofclubs.command.commands.Status;
import net.baronofclubs.botofclubs.command.commands.Time;
import net.baronofclubs.botofclubs.command.commands.Viewers;
import net.baronofclubs.botofclubs.command.commands.Uptime;

public class CommandReference {
	
	public Map<String, Command> commandStorage = new HashMap<String, Command>();
	public Map<String, String> commandAliases = new HashMap<String, String>();
	
	public CommandReference() {
		buildReference();
	}
	
	public void buildReference() {
		Command time = new Time();
		putMaps(time);
		
		Rolls rolls = new Rolls();
		putMaps(rolls);

		
		Chomsky chomsky = new Chomsky();
		putMaps(chomsky);
		
		Uptime lastLive = new Uptime();
		putMaps(lastLive);
		
		IsLive isLive = new IsLive();
		putMaps(isLive);
		
		Viewers viewers = new Viewers();
		putMaps(viewers);
		
		Status status = new Status();
		putMaps(status);
	}

	public boolean exists(String alias) {
		return commandAliases.containsKey(alias);
	}
	
	public Command get(String alias) {
		String name = commandAliases.get(alias);
		return commandStorage.get(name);
	}	
	
	private void putMaps(Command command) {
		for(String alias : command.getAliases()) {
			commandAliases.put(alias, command.getName());
		}
		commandStorage.put(command.getName(), command);
	}
	
	public JsonObject getAsJsonObject() {
		JsonObject jsonObject = new JsonObject();

		for (Map.Entry<String, Command> entry : commandStorage.entrySet()) {
			jsonObject.add(entry.getKey(), entry.getValue().getAsJsonObject());
		}

		return jsonObject;
	}

	public void fromJsonObject(JsonObject jsonObject) {;
		for (Map.Entry<String, JsonElement> entry : jsonObject.entrySet()) {
			commandStorage.get(entry.getKey()).fromJsonObject(entry.getValue().getAsJsonObject());
			putAliases(commandStorage.get(entry.getKey()));
		}
	}
	
	public void putAliases(Command command) {
		for(String alias : command.getAliases()) {
			commandAliases.put(alias, command.getName());
		}
	}
}
