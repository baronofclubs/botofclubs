package net.baronofclubs.botofclubs.command;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import net.baronofclubs.botofclubs.GlobalSettings;
import net.baronofclubs.botofclubs.server.Message;

import java.time.ZonedDateTime;
import java.util.Arrays;

import org.apache.commons.lang3.StringUtils;

public abstract class CommandUtils implements Command {

	protected boolean enabled;
	protected String name;
	protected String[] aliases;
	protected ZonedDateTime lastRun;
	protected int cooldown;
	protected int userLevel;
	protected int requiredArgs;

	// Primary Method
	@Override
	public abstract String command(Message message);
	
	// Run Command
	@Override
	public String run(Message message) {
		if (runnable(message)) {
			System.out.println(GlobalSettings.SDP + "Running Command - " + message.getCommandName());
			
			// Leave this line as setting for cool down.
			setLastRun(ZonedDateTime.now());
			return command(message);
		}
		System.out.println(GlobalSettings.SDP + "Not Running Command - " + message.getCommandName() + ". Reason: " + getNonRunnableReason(message));
		return "";
	}
	
	private String getNonRunnableReason(Message message) {
		if (message.getOriginUser().userProperties.getPermLevel() <= userLevel) {
			return "Insufficient User Permissions";
		} else if (!isCool()) {
			return "Command on Cool Down";
		} else if (!enabled()) {
			return "Command not enabled";
		} else if (!hasRequiredArgs(message)) {
			return "Not Enough Arguments";
		}
		return "Unknown Reason";
	}

	public void setProperty(String property, String newValue) {
		switch (property.toLowerCase()) {
		case "userlevel":
			if (StringUtils.isNumeric(newValue)) {
				setUserLevel(Integer.parseInt(newValue));
			}
		case "cooldown":
			if (StringUtils.isNumeric(newValue)) {
				setCooldown(Integer.parseInt(newValue));
			}
		case "disable":
			disable();
		case "enable":
			enable();
		}
	}
	
	public boolean runnable(Message message) {
		boolean userPerm = (message.getOriginUser().userProperties.getPermLevel() >= userLevel);
		
		return isCool() && userPerm && enabled() && hasRequiredArgs(message);
	}
	
	private boolean hasRequiredArgs(Message message) {
		if (requiredArgs > 0) {
			return (message.getArgs().length >= requiredArgs);
		}
		return true;
	}

	// Getters and Setters
	// Enable or Disable
	@Override
	public boolean enabled() {
		return this.enabled;
	}
	@Override
	public void enable() {
		this.enabled = true;
	}
	@Override
	public void disable() {
		this.enabled = true;
	}
	
	// Name
	@Override
	public String getName() {
		return this.name;
	}
	
	// Aliases
	@Override
	public String[] getAliases() {
		return this.aliases;
	}
	@Override
	public void addAlias(String newAlias) {
		String[] newAliases = new String[this.aliases.length + 1];
		System.arraycopy(this.aliases, 0, newAliases, 0, aliases.length);
		newAliases[this.aliases.length + 1] = newAlias;
		this.aliases = newAliases;
	}
	@Override
	public void removeAlias(String alias) {
		for (int i = 0; i <= this.aliases.length; i++) {
			if (this.aliases[i].equalsIgnoreCase(alias)) {
				String[] newAliases = new String[this.aliases.length - 1];
				System.arraycopy(this.aliases, 0, newAliases, 0, i);
				System.arraycopy(this.aliases, i+1, newAliases, i, this.aliases.length - i - 1);
				this.aliases = newAliases;
			}
		}
	}
	
	// Last run and Cool down
	@Override
	public ZonedDateTime getLastRun() {
		return this.lastRun;
	}
	@Override
	public void setLastRun(ZonedDateTime run) {
		this.lastRun = run;
	}
	@Override
	public int getCooldown(){
		return this.cooldown;
	}
	@Override
	public void setCooldown(int cdset) {
		this.cooldown = cdset;
	}
	
	// User Level stuff
	@Override
	public int getUserLevel() {
		return this.userLevel;
	}
	@Override
	public void setUserLevel(int ulset) {
		this.userLevel = ulset;
	}
	
	// Cool Down Stuff
	public boolean isCool() {
		return lastRun.plusSeconds(Integer.toUnsignedLong(cooldown)).isBefore(ZonedDateTime.now());
	}
	
	
	public int getRequiredArgs() {
		return requiredArgs;
	}

	public void setRequiredArgs(int requiredArgs) {
		this.requiredArgs = requiredArgs;
	}

	// Properties Saving Stuff
	public JsonObject getAsJsonObject() {
		JsonObject jsonObject = new JsonObject();

		jsonObject.addProperty("enabled", enabled);
		jsonObject.addProperty("name", name);
		jsonObject.addProperty("aliases", Arrays.toString(aliases));
		jsonObject.addProperty("lastRun", lastRun.toString());
		jsonObject.addProperty("cooldown", cooldown);
		jsonObject.addProperty("userLevel", userLevel);
		jsonObject.addProperty("requiredArgs", requiredArgs);
		
		return jsonObject;
	}

	public void fromJsonObject(JsonObject jsonObject) {

		this.enabled = jsonObject.get("enabled").getAsBoolean();
		this.name = jsonObject.get("name").getAsString();
		this.lastRun = ZonedDateTime.parse(jsonObject.get("lastRun").getAsString());
		this.cooldown = jsonObject.get("cooldown").getAsInt();
		this.userLevel = jsonObject.get("userLevel").getAsInt();
		this.requiredArgs = jsonObject.get("requiredArgs").getAsInt();

		Gson gson = new Gson();
		this.aliases = gson.fromJson(jsonObject.get("aliases").getAsString(), String[].class);

	}
}
