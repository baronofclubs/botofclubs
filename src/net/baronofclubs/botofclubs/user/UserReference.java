package net.baronofclubs.botofclubs.user;

import java.util.HashMap;
import java.util.Map;

import net.baronofclubs.botofclubs.GlobalSettings;
import net.baronofclubs.botofclubs.server.Server;

public class UserReference {
	
	private Server server;
	private Map<String, User> userStorage = new HashMap<String, User>();
	
	public UserReference(Server server) {
		this.server = server;
		User me = new User(GlobalSettings.defaultNick.toLowerCase(), server.serverProperties.getName());
		userStorage.put(me.userProperties.getName(), me);
		
		User baron = new User("baronofclubs", server.serverProperties.getName());
		userStorage.put("baronofclubs", baron);
		baron.userProperties.setPermLevel(999);
	}

	public User get(String nick) {
		System.out.println(GlobalSettings.SDP + "Getting User - " + nick);
		if (!userStorage.containsKey(nick)) {
			User user = new User(nick, server.serverProperties.getName());
			userStorage.put(user.userProperties.getName(), user);
			return user;
		}
		return userStorage.get(nick);
	}
	
	public void add(String nick) {
		System.out.println("Adding user");
		if (!userStorage.containsKey(nick)) {
			User user = new User(nick, server.serverProperties.getName());
			userStorage.put(user.userProperties.getName(), user);
		}
		System.out.println("added user");
	}

}
