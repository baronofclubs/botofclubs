package net.baronofclubs.botofclubs.user;

import java.io.File;
import java.nio.file.Paths;

import com.google.gson.JsonObject;

import net.baronofclubs.botofclubs.AutoSaver;
import net.baronofclubs.botofclubs.GlobalSettings;
import net.baronofclubs.botofclubs.Properties;
import net.baronofclubs.botofclubs.PropertiesFile;

public class User {

	public UserProperties userProperties;
	
	public User(String nick, String serverName) {
		System.out.println(GlobalSettings.SDP + "Creating User - " + nick);
		
		userProperties = new UserProperties(nick, serverName);
	}
	
	public class UserProperties extends PropertiesFile implements Properties {
		
		private String name;
		private String serverName;
		private int globalPermLevel;
		
		public UserProperties(String name, String serverName) {
			super(name + ".json", Paths.get("server" + File.separator + serverName + File.separator + "users" + File.separator));
			new AutoSaver(this);
			this.name = name;
			this.serverName = serverName;
			
			if(!this.load()) {
				this.create();
			}
		}

		@Override
		public void setToDefaults() {
			this.globalPermLevel = 1;
		}

		@Override
		public JsonObject getAsJsonObject() {
			JsonObject jsonObject = new JsonObject();
			jsonObject.addProperty("name", name);
			jsonObject.addProperty("serverName", serverName);
			jsonObject.addProperty("permLevel", globalPermLevel);
			return jsonObject;
		}

		@Override
		public void fromJsonObject(JsonObject jsonObject) {
			this.name = parseJsonMember(jsonObject, "name", "UNDEFINED"); // Parse String
			serverName = parseJsonMember(jsonObject, "serverName", "SERVERNAME"); // Parse String
			globalPermLevel = parseJsonMember(jsonObject, "permLevel", (int) 0); // Parse int
		}

		public String getName() {
			return name;
		}

		public String getServerName() {
			return serverName;
		}

		public int getPermLevel() {
			return globalPermLevel;
		}
		
		public void setPermLevel(int permLevel) {
			this.globalPermLevel = permLevel;
			save();
		}
	}
}
