package net.baronofclubs.botofclubs;

import java.time.ZonedDateTime;

import net.baronofclubs.botofclubs.apis.TwitchAPI;
import net.baronofclubs.botofclubs.server.Server;

public class Main {

    public static void main(String[] args) {
        System.out.println(GlobalSettings.SDP + "Starting Up");
      
        System.out.println(GlobalSettings.SDP + "Generating Settings");
        GlobalSettings globalSettings = new GlobalSettings();
        if (!globalSettings.load()) {
        	globalSettings.create();
        }
        
        System.out.println(GlobalSettings.SDP +"Generating Twtich Settings");
        GlobalSettings.Twitch twitchApiSettings = new GlobalSettings.Twitch();
        if (!twitchApiSettings.load()) {
        	twitchApiSettings.create();
        }

        // Get Servers and Connect
        Server server = new Server("Twitch");
        server.openConnection();

    }
}
