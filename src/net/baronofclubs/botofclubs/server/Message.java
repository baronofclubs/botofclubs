package net.baronofclubs.botofclubs.server;

import org.pircbotx.hooks.events.MessageEvent;

import net.baronofclubs.botofclubs.channel.Channel;
import net.baronofclubs.botofclubs.user.User;
import net.baronofclubs.botofclubs.user.UserReference;

public class Message {
	
	private Server server;
	private Channel channel;
	public Channel getChannel() {
		return channel;
	}
	
	private User originUser;
	private char commandPrefix;
	private char adminPrefix;
	
	private String rawMessage;
	private char firstChar;
	private char secondChar;
	private String commandName;
	
	//private boolean isCommand;
	
	public Message(MessageHandler messageHandler, MessageEvent event) {
		this.server = messageHandler.getServer();
		this.channel = server.getChannelReference().get(event.getChannel().getName().substring(1));

		this.originUser = server.getUserReference().get(event.getUser().getNick());
		this.rawMessage = event.getMessage();
		this.firstChar = event.getMessage().charAt(0);
		this.secondChar = event.getMessage().charAt(1);
		
		this.commandPrefix = channel.channelProperties.getCommandPrefix();
		this.adminPrefix = channel.channelProperties.getAdminPrefix();
		
		this.commandName = processCommandName();
		
		
		//this.isCommand = verifyCommand();
	}
	
	private boolean verifyCommand() {
		return channel.channelProperties.getCommandReference().exists(commandName);
	}

	public String processCommandName() {
		int dropChars = 0;
		int spaceAt = 0;
		if (hasCommandPrefix() && !hasMultiPrefix()) {
			dropChars = 1;
		} else if (hasCommandPrefix() && hasMultiPrefix()) {
			dropChars = 2;
		}
		if (rawMessage.contains(" ")) {
			spaceAt = rawMessage.indexOf(" ");
			return rawMessage.substring(dropChars, spaceAt).toLowerCase();
		} else {
			return rawMessage.substring(dropChars).toLowerCase();
		}
	}
	
	public String[] getArgs() {
		if (rawMessage.contains(" ")) {
			return rawMessage.substring(rawMessage.indexOf(" ") + 1).split(" ");
		}
		return null;
	}
	
	public String getArgString() {
		if (rawMessage.contains(" ")) {
			return rawMessage.substring(rawMessage.indexOf(" ") + 1);
		}
		return null;
	}
	
	public boolean hasCommandPrefix() {
		if (firstChar == commandPrefix || 
				firstChar == adminPrefix) {
			return true;
		}
		return false;
	}
	
	public boolean hasMultiPrefix() {
		if (hasCommandPrefix() &&
				(secondChar == commandPrefix || 
				secondChar == adminPrefix)) {
			return true;
		}
		return false;
	}
	
	public String getPrefixType() {
		if (hasCommandPrefix() && !hasMultiPrefix()) {
			if (firstChar == commandPrefix) {
				return "Command";
			} else if (firstChar == adminPrefix) {
				return "Admin";
			}
		} else if (hasCommandPrefix() && hasMultiPrefix()) {
			if (firstChar == commandPrefix) {
				if (secondChar == adminPrefix) {
					return "CommandAdmin";
				} else if (secondChar == commandPrefix) {
					return "CommandCommand";
				}
			} else if (firstChar == adminPrefix) {
				if (secondChar == adminPrefix) {
					return "AdminAdmin";
				} else if (secondChar == commandPrefix) {
					return "AdminCommand";
				}
			}
		} else {
			return "None";
		}
		return null;
	}
	
	public boolean containsAutoReplyKeywords() {
		return false;
	}

	public User getOriginUser() {
		return originUser;
	}

	public void setOriginUser(User originUser) {
		this.originUser = originUser;
	}

	public char getCommandPrefix() {
		return commandPrefix;
	}

	public void setCommandPrefix(char commandPrefix) {
		this.commandPrefix = commandPrefix;
	}

	public char getAdminPrefix() {
		return adminPrefix;
	}

	public void setAdminPrefix(char adminPrefix) {
		this.adminPrefix = adminPrefix;
	}

	public String getRawMessage() {
		return rawMessage;
	}

	public void setRawMessage(String rawMessage) {
		this.rawMessage = rawMessage;
	}

	public char getFirstChar() {
		return firstChar;
	}

	public void setFirstChar(char firstChar) {
		this.firstChar = firstChar;
	}

	public char getSecondChar() {
		return secondChar;
	}

	public void setSecondChar(char secondChar) {
		this.secondChar = secondChar;
	}

	public String getCommandName() {
		return commandName;
	}
	
	// Should not ever need this.
	//public void setCommandName(String commandName) {
	//	this.commandName = commandName;
	//}

	public boolean isCommand() {
		return getPrefixType().equals("Command") && verifyCommand();
	}

	//public void setCommand(boolean isCommand) {
	//	this.isCommand = isCommand;
	//}

	public boolean isCommandAdmin() {
		return getPrefixType().equals("CommandAdmin") && verifyCommand();
	}

	public boolean isAdminCommand() {
		return getPrefixType().equals("AdminCommand");
	}			
}
