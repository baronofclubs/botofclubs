package net.baronofclubs.botofclubs.server;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.pircbotx.Configuration;
import org.pircbotx.PircBotX;
import org.pircbotx.cap.EnableCapHandler;
import org.pircbotx.exception.IrcException;

import com.google.gson.JsonObject;

import net.baronofclubs.botofclubs.AutoSaver;
import net.baronofclubs.botofclubs.GlobalSettings;
import net.baronofclubs.botofclubs.Properties;
import net.baronofclubs.botofclubs.PropertiesFile;
import net.baronofclubs.botofclubs.channel.Channel;
import net.baronofclubs.botofclubs.channel.ChannelReference;
import net.baronofclubs.botofclubs.command.CommandReference;
import net.baronofclubs.botofclubs.user.UserReference;

public class Server {

	public Configuration serverConfig;
	public ServerProperties serverProperties;
	private PircBotX pircBotX;
	
	private MessageHandler messageHandler;
	private ChannelReference channelReference;
	private UserReference userReference;
	private CommandReference commandReference;
	
	private String name;
	
	public Server(String name) {
		System.out.println(GlobalSettings.SDP + "Creating Server - " + name);
		
		this.name = name;
		this.serverProperties = new ServerProperties(this);
		this.channelReference = new ChannelReference(this);
		this.userReference = new UserReference(this);
		this.messageHandler = new MessageHandler(this);
		this.pircBotX = new PircBotX(configure());
	}
	
	public void openConnection() {
		try {
			pircBotX.startBot();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (IrcException e) {
			e.printStackTrace();
		}
	}

	public Configuration configure() {
		Configuration.Builder configurationBuilder = new Configuration.Builder();
		configurationBuilder.setName(serverProperties.getDefaultNick());
		configurationBuilder.setLogin(serverProperties.getUsername());
		configurationBuilder.setServerPassword(serverProperties.getPassword());
		configurationBuilder.setAutoNickChange(false); // TODO: Add this to serverProperties
		
		configurationBuilder.setEncoding(GlobalSettings.ENCODING);
		configurationBuilder.setMessageDelay(1500); // TODO: Add this to serverProperties
		configurationBuilder.setRealName(GlobalSettings.VERSION);
		configurationBuilder.addServer(serverProperties.getServerAddress().toString(), serverProperties.getPort());
		
		configurationBuilder.setOnJoinWhoEnabled(false); // TODO: Add this to serverProperties
		configurationBuilder.setCapEnabled(true); // TODO: Add this to serverProperties
		configurationBuilder.addCapHandler(new EnableCapHandler("twitch.tv/membership")); // TODO: Add this to serverProperties
		configurationBuilder.addCapHandler(new EnableCapHandler("twitch.tv/tags")); // TODO: Add this to serverProperties
		//configurationBuilder.setSocketFactory(SSLSocketFactory.getDefault()); // TODO: Add this to serverProperties
		//configurationBuilder.setSocketFactory(new UtilSSLSocketFactory().trustAllCertificates()); // TODO: Add this to serverProperties
		//configurationBuilder.setSocketFactory(new UtilSSLSocketFactory().disableDiffieHellman()); // TODO: Add this to serverProperties
		configurationBuilder.setAutoReconnect(true); // TODO: Add this to serverProperties
		configurationBuilder.setAutoReconnectDelay(5); // TODO: Add this to serverProperties
		configurationBuilder.setAutoReconnectAttempts(10); // TODO: Add this to serverProperties
		configurationBuilder.addListener(messageHandler);
		
		configurationBuilder.addAutoJoinChannel(serverProperties.getDefaultChannel().channelProperties.getChannelToken());
		for (Channel channel : getAutoJoinChannels()) {
			
			configurationBuilder.addAutoJoinChannel(channel.channelProperties.getChannelToken());
		}

		return configurationBuilder.buildConfiguration();
	}
	
	public void joinChannel(Channel channel) {
		channelReference.add(channel);
		pircBotX.send().joinChannel(channel.channelProperties.getChannelToken());
	}

	public Configuration getServerConfig() {
		return serverConfig;
	}

	public ServerProperties getServerProperties() {
		return serverProperties;
	}

	public ChannelReference getChannelReference() {
		return channelReference;
	}

	public UserReference getUserReference() {
		return userReference;
	}

	public CommandReference getCommandReference() {
		return commandReference;
	}
	
	public void sendMessage(Channel channel, String message) {
		pircBotX.send().message(channel.channelProperties.getChannelToken(), message);
	}

	public void partChannel(Channel channel) {
		pircBotX.sendRaw().rawLine("PART " + channel.channelProperties.getChannelToken());;	
	}

	public String getName() {
		return name;
	}

	public List<Channel> getAutoJoinChannels() {
		List<Channel> autoJoiners = new ArrayList<Channel>();
		for (Channel channel : channelReference.getAllAsList()) {
			if (channel.channelProperties.isAutoJoin()) {
				autoJoiners.add(channel);
			}
		}
		return autoJoiners;
	}
	
	public class ServerProperties extends PropertiesFile implements Properties {

		private String name;
		private URI serverAddress;
		private int port;
		private String defaultNick;
		private String username;
		private String password;
		private Channel defaultChannel;
		private boolean autoConnect;
		
		private Server server;
		
		
		
		public ServerProperties(Server server) {
			super(server.getName() + ".json", Paths.get("server" + File.separator + server.getName() + File.separator));
			new AutoSaver(this);
			this.name = server.getName();
			this.server = server;
			this.defaultChannel = new Channel(GlobalSettings.defaultNick.toLowerCase(), server);
			
			if(!this.load()) {
				this.create();
			}
		}

		@Override
		// TODO: Get rid of this default Twitch bullshit
		public void setToDefaults() {
			this.serverAddress = URI.create("irc.chat.twitch.tv");
			this.port = 6667;
			this.defaultNick = GlobalSettings.defaultNick;
			this.username = GlobalSettings.defaultNick;
			this.password = "oauth:xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
			this.autoConnect = true;
		}

		@Override
		public JsonObject getAsJsonObject() {
			
			JsonObject jsonObject = new JsonObject();
			jsonObject.addProperty("name", this.name);
			jsonObject.addProperty("serverAddress", this.serverAddress.toString());
			jsonObject.addProperty("port", this.port);
			jsonObject.addProperty("defaultNick", this.defaultNick);
			jsonObject.addProperty("username", this.username);
			jsonObject.addProperty("password", this.password);
			jsonObject.addProperty("autoConnect", this.autoConnect);
			//jsonObject.add("autoJoinChannels", server.getChannelReference().getAutoJoinAsJson());
			return jsonObject;
		}

		@Override
		public void fromJsonObject(JsonObject jsonObject) {

			this.name = parseJsonMember(jsonObject, "name", "UNDEFINED"); // Parse String
			this.serverAddress = parseJsonMember(jsonObject, "serverAddress", URI.create("irc.serveraddress.example")); // Parse URI
			this.port = parseJsonMember(jsonObject, "port", (int) 0); // Parse int
			this.defaultNick = parseJsonMember(jsonObject, "defaultNick", "NICK"); // Parse String
			this.username = parseJsonMember(jsonObject, "username", "USERNAME"); // Parse String
			this.password = parseJsonMember(jsonObject, "password", "PASSWORD"); // Parse String
			this.autoConnect = parseJsonMember(jsonObject, "autoConnect", false); // Parse boolean
			//server.getChannelReference().autoJoinFromJson(jsonObject.get("autoJoinChannels").getAsJsonObject());
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public URI getServerAddress() {
			return serverAddress;
		}

		public void setServerAddress(URI serverAddress) {
			this.serverAddress = serverAddress;
		}

		public int getPort() {
			return port;
		}

		public void setPort(int port) {
			this.port = port;
		}

		public String getDefaultNick() {
			return defaultNick;
		}

		public void setDefaultNick(String defaultNick) {
			this.defaultNick = defaultNick;
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public boolean isAutoConnect() {
			return autoConnect;
		}

		public void setAutoConnect(boolean autoConnect) {
			this.autoConnect = autoConnect;
		}

		public Channel getDefaultChannel() {
			return defaultChannel;
		}

		public void setDefaultChannel(Channel defaultChannel) {
			this.defaultChannel = defaultChannel;
		}

	}
}
