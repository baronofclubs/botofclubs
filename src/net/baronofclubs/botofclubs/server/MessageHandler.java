package net.baronofclubs.botofclubs.server;

import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.MessageEvent;

import net.baronofclubs.botofclubs.GlobalSettings;
import net.baronofclubs.botofclubs.channel.Channel;
import net.baronofclubs.botofclubs.command.Command;

import java.util.Arrays;

public class MessageHandler extends ListenerAdapter {
	private Server server;

	public MessageHandler(Server server) {
		this.server = server;
	}
	
	public void onMessage(MessageEvent event) {
		Message message = new Message(this, event);
		Channel channel = message.getChannel();
		
		channel.channelProperties.getChannelUser(event.getUser().getNick()).chatted();
		channel.channelProperties.save();
		
		System.out.println(GlobalSettings.SDP + "Message - " + message.getPrefixType() + ": '" + message.getCommandName() + "' - Args: " + Arrays.toString(message.getArgs()));
		
		Command command;
		if (message.isCommand()) {
			command = message.getChannel().channelProperties.getCommandReference().get(message.getCommandName());
			String response = command.run(message);
			if (!response.equals("")) {
				event.respondWith(response);
			}
			
		} else if (message.isCommandAdmin()) {
			command = message.getChannel().channelProperties.getCommandReference().get(message.getCommandName());
			String[] args = message.getArgs();
			command.setProperty(args[0], args[1]);
			message.getChannel().channelProperties.save();
			event.respondWith(message.getCommandName() + " changed.");
			
		} else if (message.isAdminCommand()) {
			String arg0 = "";
			if (!(message.getArgs() == null)) {
				arg0 = message.getArgs()[0];
			}
			
			switch (message.getCommandName().toLowerCase()) {
			case "prefix":
				channel = message.getChannel();
				if (arg0.length() == 1) {
					channel.channelProperties.setCommandPrefix(arg0.charAt(0));
					event.respondWith("Command Prefix is now '" + channel.channelProperties.getCommandPrefix() + "'");
				} else {
					event.respondWith("Command Prefix is '" + channel.channelProperties.getCommandPrefix() + "'");
				}
				break;
				
			case "join":
				Channel newChannel;
				if (server.getChannelReference().containsName(arg0)) {
					newChannel = server.getChannelReference().get(arg0);
					newChannel.channelProperties.setAutoJoin(true);
				} else {
					newChannel = new Channel(arg0, server);
				}

				server.joinChannel(newChannel);
				server.sendMessage(newChannel, "Hi! " + message.getOriginUser().userProperties.getName() + " sent me.");
				event.respondWith("I went ahead and joined " + arg0 + " for you.");
				break;
				
			case "part":
				message.getChannel().channelProperties.setAutoJoin(false);
				server.partChannel(message.getChannel());
				break;
			}
		}
	}

	public Server getServer() {
		return server;
	}

	
	
	
	
}
