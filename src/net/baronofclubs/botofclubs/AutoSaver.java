package net.baronofclubs.botofclubs;

import java.time.ZonedDateTime;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class AutoSaver {

	private Properties properties;
	
	Runnable saver = new Runnable() {
		@Override
		public void run() {
			try {
				long saveInterval = Integer.toUnsignedLong(GlobalSettings.saveInterval);
				if (properties.getLastSave().isBefore(ZonedDateTime.now().minusMinutes(saveInterval))) {
					properties.save();
				}
	            
	        } 
	        catch (Exception e) {
	            e.printStackTrace();
	        }
		}
	};
	
	
	public AutoSaver(Properties properties) {
		this.properties = properties;
		
		long startDelay = 1;
		long saveInterval = Integer.toUnsignedLong(GlobalSettings.saveInterval);
		
		ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        
        executor.scheduleAtFixedRate(saver, startDelay, saveInterval, TimeUnit.MINUTES);
	}
}
