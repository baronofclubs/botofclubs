package net.baronofclubs.botofclubs;

// Properties File - Parent Class for saving and loading various properties files into/from Json.

import java.io.*;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.UUID;

import com.google.gson.*;


public class PropertiesFile implements Properties {

    private final int LOGGING_LEVEL = 1; // TODO: Unused LOGGING_LEVEL. Implement console printouts for status.
    private ZonedDateTime lastSave;
    private String fileName;
    private Path filePath;
    
    public PropertiesFile(String fileName, Path filePath) {
    	this.fileName = fileName;
    	this.filePath = filePath;
    }

    // Create a file with default properties, as defined in subclass method setToDefaults()
    protected boolean create() {
    	System.out.println(GlobalSettings.SDP + "Creating Properties File - " + filePath + File.separator + fileName);
        setToDefaults();
        JsonObject jsonObject = getAsJsonObject();
        
        if (!exists()) {
            createBlank();
        }
        
        lastSave = ZonedDateTime.now();
        jsonObject.addProperty("lastSave", lastSave.toString());

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String jsonString = gson.toJson(jsonObject);

        writeString(jsonString);
        return true;
    }

    // Save an already existing file. If file doesn't exist, set to defaults and create.
    public boolean save() {
    	System.out.println(GlobalSettings.SDP + "Saving Properties File - " + filePath + File.separator + fileName);
    	
        JsonObject jsonObject;
        try {
            jsonObject = getAsJsonObject();
        } catch (NullPointerException e) {
            setToDefaults();
            jsonObject = getAsJsonObject();
        }

        if (!exists()) {
            createBlank();
        }
        
        lastSave = ZonedDateTime.now();
        jsonObject.addProperty("lastSave", lastSave.toString());

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String jsonString = gson.toJson(jsonObject);

        writeString(jsonString);
        return true;
    }

    // Read properties from file.
    public boolean read() {
    	System.out.println(GlobalSettings.SDP + "Reading Properties File - " + filePath + File.separator + fileName);
        Path fullPath = Paths.get(filePath.toString() + File.separator + fileName);

        InputStream inputStream;
        try {
            inputStream = new FileInputStream(fullPath.toString());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        }
        Reader inputStreamReader = new InputStreamReader(inputStream);

        JsonParser jsonParser = new JsonParser();
        JsonElement jsonElement = jsonParser.parse(inputStreamReader);
        JsonObject jsonObject = jsonElement.getAsJsonObject();

        lastSave = ZonedDateTime.parse(jsonObject.get("lastSave").getAsString());
        fromJsonObject(jsonObject);

        return true;
    }

    public boolean load() {
    	System.out.println(GlobalSettings.SDP + "Loading Properties File - " + filePath + File.separator + fileName);
        Path fullPath = Paths.get(filePath.toString() + File.separator + fileName);

        InputStream inputStream;
        try {
            inputStream = new FileInputStream(fullPath.toString());
        } catch (FileNotFoundException e) {
            //e.printStackTrace();
            return false;
        }
        Reader inputStreamReader = new InputStreamReader(inputStream);

        JsonParser jsonParser = new JsonParser();
        JsonElement jsonElement = jsonParser.parse(inputStreamReader);
        JsonObject jsonObject = jsonElement.getAsJsonObject();

        fromJsonObject(jsonObject);

        return true;
    }

    // Delete a properties file. TODO: implement deletion of properties files
    public boolean delete() {
    	System.out.println(GlobalSettings.SDP + "Deleting Properties File - " + filePath + File.separator + fileName);
        Path fullPath = Paths.get(filePath.toString() + File.separator + fileName);
        File file = new File(fullPath.toString());
        return file.delete();

    }

    public boolean copy() {
    	System.out.println(GlobalSettings.SDP + "Copying Properties File - " + filePath + File.separator + fileName);
        // TODO: Add Copy Code
        return true;
    }

    // Check if a file exists
    public boolean exists() {
        Path fullPath = Paths.get(filePath.toString() + File.separator + fileName);
        File file = new File(fullPath.toString());
        return file.exists();
    }

    // Write a pre-generated String to file.
    private boolean writeString(String string) {
        Path fullPath = Paths.get(filePath.toString() + File.separator + fileName);
        try {
            Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fullPath.toString()), GlobalSettings.ENCODING));
            writer.write(string);
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    // Create a blank file.
    private boolean createBlank() {
        Path fullPath = Paths.get(filePath.toString() + File.separator + fileName);
        try {
            File file = new File(fullPath.toString());
            file.getParentFile().mkdirs();
            file.createNewFile();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
    
    public ZonedDateTime getLastSave() {
    	if (lastSave == null) {
    		return ZonedDateTime.now();
    	}
    	
    	return lastSave;
    }

	@Override
	public void setToDefaults() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public JsonObject getAsJsonObject() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void fromJsonObject(JsonObject jsonObject) {
		// TODO Auto-generated method stub
		
	}
	
	protected static String parseJsonMember(JsonObject jsonObject, String jsonElementName, String defaultValue) {
		JsonElement jsonElement = jsonObject.get(jsonElementName);
		return (jsonElement instanceof JsonNull) ? defaultValue : jsonElement.getAsString();
	}
	
	protected static boolean parseJsonMember(JsonObject jsonObject, String jsonElementName, boolean defaultValue) {
		JsonElement jsonElement = jsonObject.get(jsonElementName);
		return (jsonElement instanceof JsonNull) ? defaultValue : jsonElement.getAsBoolean();
	}
	
	protected static long parseJsonMember(JsonObject jsonObject, String jsonElementName, long defaultValue) {
		JsonElement jsonElement = jsonObject.get(jsonElementName);
		return (jsonElement instanceof JsonNull) ? defaultValue : jsonElement.getAsLong();
	}
	
	protected static int parseJsonMember(JsonObject jsonObject, String jsonElementName, int defaultValue) {
		JsonElement jsonElement = jsonObject.get(jsonElementName);
		return (jsonElement instanceof JsonNull) ? defaultValue : jsonElement.getAsInt();
	}
	
	protected static char parseJsonMember(JsonObject jsonObject, String jsonElementName, char defaultValue) {
		JsonElement jsonElement = jsonObject.get(jsonElementName);
		return (jsonElement instanceof JsonNull) ? defaultValue : jsonElement.getAsCharacter();
	}
	
	protected static ZonedDateTime parseJsonMember(JsonObject jsonObject, String jsonElementName, ZonedDateTime defaultValue) {
		JsonElement jsonElement = jsonObject.get(jsonElementName);
		return (jsonElement instanceof JsonNull) ? defaultValue : ZonedDateTime.parse(jsonElement.getAsString());
	}
	
	protected static URI parseJsonMember(JsonObject jsonObject, String jsonElementName, URI defaultValue) {
		JsonElement jsonElement = jsonObject.get(jsonElementName);
		return (jsonElement instanceof JsonNull) ? defaultValue : URI.create(jsonElement.getAsString());
	}
	
	protected static UUID parseJsonMember(JsonObject jsonObject, String jsonElementName, UUID defaultValue) {
		JsonElement jsonElement = jsonObject.get(jsonElementName);
		return (jsonElement instanceof JsonNull) ? defaultValue : UUID.fromString(jsonElement.getAsString());
	}
	
	protected static ZoneId parseJsonMember(JsonObject jsonObject, String jsonElementName, ZoneId defaultValue) {
		JsonElement jsonElement = jsonObject.get(jsonElementName);
		return (jsonElement instanceof JsonNull) ? defaultValue : ZoneId.of(jsonElement.getAsString());
	}
	
	protected static boolean jsonMemberIsNull(JsonObject jsonObject, String jsonElementName) {
		JsonElement jsonElement = jsonObject.get(jsonElementName);
		return (jsonElement instanceof JsonNull);
	}

}
