package net.baronofclubs.botofclubs.apis;

import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.time.ZonedDateTime;
import java.util.Map;

import org.apache.http.HttpStatus;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

public class TwitchAPI {

	public static String VERSION = "BotOfClubs-Twitch-0.2.2";
	private static URI BASE_URL = URI.create("https://api.twitch.tv/kraken");
	private static String REQ_VERSION = "application/vnd.twitchtv.v3+json";
	private boolean authenticatedSession;
	
	public TwitchAPI(String clientID, String authorization) {
		System.out.println("TWITCHAPI: Starting TwitchAPI vers" + VERSION);
		Unirest.setDefaultHeader("Client-ID", clientID);
		Unirest.setDefaultHeader("Authorization", "OAuth " + authorization);
		Unirest.setDefaultHeader("Accept", REQ_VERSION);
		authenticatedSession = true;
	}
	
	public TwitchAPI(String clientID) {
		System.out.println("TWITCHAPI: Starting TwitchAPI vers" + VERSION);
		Unirest.setDefaultHeader("Client-ID", clientID);
		Unirest.setDefaultHeader("Accept", REQ_VERSION);
		authenticatedSession = false;
	}
	
	public URI getLink(String linkType, URI fromURL) {
		String linksPropertyName = "_links";
		
		JsonObject linksObject = getJsonObject(linksPropertyName, fromURL, HttpStatusCode.OK);
		
		return URI.create(linksObject.get(linkType).getAsString());
	}
	
	public Channel getChannel(String channelName) {
		System.out.println("TWITCHAPI: Getting Channel - " + channelName);
		return new Channel(channelName);
	}
	
	public Stream getStream(String channelName) {
		System.out.println("TWITCHAPI: Getting Stream - " + channelName);
		return new Stream(channelName);
	}
	
	private JsonObject getJsonObject(URI fromURI, int expectedHttpStatus) {
		String jsonResponse = "";
		HttpResponse<String> jsonString = null;
		try {
			System.out.println("TWITCHAPI: Getting json from " + fromURI.toString());
			jsonString = Unirest.get(fromURI.toString()).asString();
			
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (!(jsonString == null)) {
			if (jsonString.getStatus() == expectedHttpStatus) {
				JsonParser jsonParser = new JsonParser();
				jsonResponse = jsonString.getBody();
				JsonObject jsonObject = jsonParser.parse(jsonResponse).getAsJsonObject();
				System.out.println("TWITCHAPI: Got json from " + fromURI.toString());
				return jsonObject;
			}
			System.out.println("TWITCHAPI: Http response error [Expected " + expectedHttpStatus + ", Got " + jsonString.getStatus() + "]");
		}
		System.out.println("TWITCHAPI: Response null while getting json from " + fromURI.toString());
		return null;
	}

	private JsonObject getJsonObject(String propertyName, URI fromURI, int expectedHttpStatus) {
		String jsonResponse = "";
		HttpResponse<String> jsonString = null;
		try {
			System.out.println("TWITCHAPI: Getting " + propertyName + " from " + fromURI.toString());
			jsonString = Unirest.get(fromURI.toString()).asString();
			
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (!(jsonString == null)) {
			if (jsonString.getStatus() == HttpStatusCode.OK) {
				JsonParser jsonParser = new JsonParser();
				jsonResponse = jsonString.getBody();
				JsonObject jsonObject = jsonParser.parse(jsonResponse).getAsJsonObject();
				System.out.println("TWITCHAPI: Got " + propertyName + " from " + fromURI.toString());
				return jsonObject.get(propertyName).getAsJsonObject();
			}
			System.out.println("TWITCHAPI: Http response error [Expected " + expectedHttpStatus + ", Got " + jsonString.getStatus() + "]");
		}
		System.out.println("TWITCHAPI: Response null while getting " + propertyName + " from " + fromURI.toString());
		return null;
	}

	public void end() {
		try {
			Unirest.shutdown();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	/* ==============
	 * Custom Classes
	 * ==============
	 */
	
	public class Channel {
		
		private URI commonURI = getLink("channel", BASE_URL);
		private URI channelURI;
		
		private Map<String, Object> jsonMap;
		
		public Channel(String channelName) {
			
			final int HTTP_EXPECTED = HttpStatusCode.OK;			
			this.channelURI = URI.create(commonURI + "s/" + channelName);
			
			JsonObject jsonObject = getJsonObject(channelURI, HTTP_EXPECTED);
			jsonMap = new Gson().fromJson(jsonObject, Map.class);
			System.out.println("TWITCHAPI: Got Channel - " + channelName);
		}

		// GET requests
		
		public boolean isMature() {
			if (jsonMap.containsKey("mature")) {
				return (boolean) jsonMap.get("mature");
			}
			return false;
		}

		public String getStatus() {
			if (jsonMap.containsKey("status")) {
				return (String) jsonMap.get("status");
			}
			return null;
		}

		public String getBroadcasterLanguage() {
			if (jsonMap.containsKey("broadcaster_language")) {
				return (String) jsonMap.get("broadcaster_language");
			}
			return null;
		}

		public String getDisplayName() {
			if (jsonMap.containsKey("display_name")) {
				return (String) jsonMap.get("display_name");
			}
			return null;
		}

		public String getGame() {
			if (jsonMap.containsKey("game")) {
				return (String) jsonMap.get("game");
			}
			return null;
		}

		public String getLanguage() {
			if (jsonMap.containsKey("language")) {
				return (String) jsonMap.get("language");
			}
			return null;
		}

		public long getId() {
			if (jsonMap.containsKey("_id")) {
				return (long) jsonMap.get("_id");
			}
			return -1;
		}

		public String getName() {
			if (jsonMap.containsKey("name")) {
				return (String) jsonMap.get("name");
			}
			return null;
		}

		public ZonedDateTime getCreatedAt() {
			if (jsonMap.containsKey("created_at")) {
				return ZonedDateTime.parse((String) jsonMap.get("created_at"));
			}
			return null;
		}

		public ZonedDateTime getUpdatedAt() {
			if (jsonMap.containsKey("updated_at")) {
				return ZonedDateTime.parse((String) jsonMap.get("updated_at"));
			}
			return null;
		}

		public int getDelay() {
			if (jsonMap.containsKey("delay")) {
				return (int) jsonMap.get("delay");
			}
			return -1;
		}

		public URI getLogo() {
			if (jsonMap.containsKey("logo")) {
				return URI.create((String) jsonMap.get("logo"));
			}
			return null;
		}

		public URI getBanner() {
			if (jsonMap.containsKey("banner")) {
				return URI.create((String) jsonMap.get("banner"));
			}
			return null;
		}

		public URI getVideoBanner() {
			if (jsonMap.containsKey("video_banner")) {
				return URI.create((String) jsonMap.get("video_banner"));
			}
			return null;
		}

		public URI getBackground() {
			if (jsonMap.containsKey("background")) {
				return URI.create((String) jsonMap.get("background"));
			}
			return null;
		}

		public URI getProfileBanner() {
			if (jsonMap.containsKey("profile_banner")) {
				return URI.create((String) jsonMap.get("profile_banner"));
			}
			return null;
		}

		public URI getProfileBannerBackgroundColor() {
			if (jsonMap.containsKey("profile_banner_background_color")) {
				return URI.create((String) jsonMap.get("profile_banner_background_color"));
			}
			return null;
		}

		public boolean isPartner() {
			if (jsonMap.containsKey("partner")) {
				return (boolean) jsonMap.get("partner");
			}
			return false;
		}

		public URI getUrl() {
			if (jsonMap.containsKey("url")) {
				return URI.create((String) jsonMap.get("url"));
			}
			return null;
		}

		public long getViews() {
			if (jsonMap.containsKey("views")) {
				return (long) jsonMap.get("views");
			}
			return -1;
		}

		public long getFollowers() {
			if (jsonMap.containsKey("followers")) {
				return (long) jsonMap.get("followers");
			}
			return -1;
		}
		
		// PUT Requests
		
		public boolean setStatus(String newStatus) {
			HttpResponse<String> response = null;
			
			try {
				response = Unirest.put(channelURI.toString()).field("channel[status]", newStatus).asString();
			} catch (UnirestException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (response.getStatus() == HttpStatusCode.OK) {
				return true;
			}
			return false;
		}
		
	}

	public class Stream {
		
		private URI commonURI = getLink("streams", BASE_URL);
		private URI channelURI;
		
		private Map<String, Object> jsonMap;
		
		public Stream(String channelName) {
			final int HTTP_EXPECTED = HttpStatusCode.OK;
			if (authenticatedSession) {
				this.channelURI = commonURI;
			} else {
				this.channelURI = URI.create(commonURI + "/" + channelName);
			}
			
			JsonObject jsonObject = getJsonObject(channelURI, HTTP_EXPECTED);
			if (!(jsonObject.get("stream").toString().equals("null"))) {
				jsonObject = jsonObject.get("stream").getAsJsonObject();
			}
			System.out.println("TWITCHAPI: Got json :" + jsonObject.toString());
			jsonMap = new Gson().fromJson(jsonObject, Map.class);
			System.out.println("TWITCHAPI: Got Stream - " + channelName);
		}
		
		public String getGame() {
			if (jsonMap.containsKey("game")) {
				return (String) jsonMap.get("game");
			}
			return null;
		}
		
		public double getViewers() {
			if (jsonMap.containsKey("viewers")) {
				return (double) jsonMap.get("viewers");
			}
			return 0;
		}
		
		public ZonedDateTime getCreatedAt() {
			if (jsonMap.containsKey("created_at")) {
				return ZonedDateTime.parse(jsonMap.get("created_at").toString());
			}
			return null;
		}
		
		public int getVideoHeight() {
			if (jsonMap.containsKey("video_height")) {
				return (int) jsonMap.get("video_height");
			}
			return -1;
		}
		
		public double getAverageFPS() {
			if (jsonMap.containsKey("average_fps")) {
				return (long) jsonMap.get("average_fps");
			}
			return -1;
		}
		
		public int getDelay() {
			if (jsonMap.containsKey("delay")) {
				return (int) jsonMap.get("delay");
			}
			return -1;
		}
		
		public boolean isPlaylist() {
			if (jsonMap.containsKey("is_playlist")) {
				return (boolean) jsonMap.get("is_playlist");
			}
			return false;
		}
		
		public boolean isLive() {
			return jsonMap.containsKey("_id"); // TODO: Change this to make more sense.
		}
		
	}
	
	
	/* ===============
	 * Utility Classes
	 * ===============
	 */
	
	public static class Authenticate {
		
	}
	
	
	public static class HttpStatusCode {
		public static final int CONTINUE = 100;
		public static final	int SWITCHING_PROTOCOLS = 101;
		public static final	int PROCESSING = 102;
		public static final	int OK = 200;
		public static final	int CREATED = 201;
		public static final	int ACCEPTED = 202;
		public static final	int NON_AUTHORITATIVE_INFORMATION = 203;
		public static final	int NO_CONTENT = 204;
		public static final	int RESET_CONTENT = 205;
		public static final	int PARTIAL_CONTENT = 206;
		public static final	int MULTI_STATUS = 207;
		public static final	int ALREADY_REPORTED = 208;
		public static final	int IM_USED = 226;
		public static final	int MULTIPLE_CHOICES = 300;
		public static final	int MOVED_PERMANENTLY = 301;
		public static final	int FOUND = 302;
		public static final	int SEE_OTHER = 303;
		public static final	int NOT_MODIFIED = 304;
		public static final	int USE_PROXY = 305;
		public static final	int TEMPORARY_REDIRECT = 307;
		public static final	int PERMANENT_REDIRECT = 308;
		public static final	int BAD_REQUEST = 400;
		public static final	int UNAUTHORIZED = 401;
		public static final	int PAYMENT_REQUIRED = 402;
		public static final	int FORBIDDEN = 403;
		public static final	int NOT_FOUND = 404;
		public static final	int METHOD_NOT_ALLOWED = 405;
		public static final	int NOT_ACCEPTABLE = 406;
		public static final	int PROXY_AUTHENTICATION_REQUIRED = 407;
		public static final	int REQUEST_TIMEOUT = 408;
		public static final	int CONFLICT = 409;
		public static final	int GONE = 410;
		public static final	int LENGTH_REQUIRED = 411;
		public static final	int PRECONDITION_FAILED = 412;
		public static final	int PAYLOAD_TOO_LARGE = 413;
		public static final	int URI_TOO_LONG = 414;
		public static final	int UNSUPPORTED_MEDIA_TYPE = 415;
		public static final	int RANGE_NOT_SATISFIABLE = 416;
		public static final	int EXPECTATION_FAILED = 417;
		public static final	int MISDIRECTED_REQUEST = 421;
		public static final	int UNPROCESSABLE_ENTITY = 422;
		public static final	int LOCKED = 423;
		public static final	int FAILED_DEPENDENCY = 424;
		public static final	int UPGRADE_REQUIRED = 426;
		public static final	int PRECONDITION_REQUIRED = 428;
		public static final	int TOO_MANY_REQUESTS = 429;
		public static final	int REQUEST_HEADER_FIELDS_TOO_LARGE = 431;
		public static final	int UNAVAILABLE_FOR_LEGAL_REASONS = 451;
		public static final	int INTERNAL_SERVER_ERROR = 500;
		public static final	int NOT_IMPLEMENTED = 501;
		public static final	int BAD_GATEWAY = 502;
		public static final	int SERVICE_UNAVAILABLE = 503;
		public static final	int GATEWAY_TIMEOUT = 504;
		public static final	int HTTP_VERSION_NOT_SUPPORTED = 505;
		public static final	int VARIANT_ALSO_NEGOTIATES = 506;
		public static final	int INSUFFICIENT_STORAGE = 507;
		public static final	int LOOP_DETECTED = 508;
		public static final	int NOT_EXTENDED = 510;
		public static final	int NETWORK_AUTHENTICATION_REQUIRED = 511;
		
	}
	
	public static class JsonTools {
		private static String jsonItemParser(JsonObject jsonObject, String jsonElementName, String defaultValue) {
			JsonElement jsonElement = jsonObject.get(jsonElementName);
			return (jsonElement instanceof JsonNull) ? defaultValue : jsonElement.getAsString();
		}
		
		private static boolean jsonItemParser(JsonObject jsonObject, String jsonElementName, boolean defaultValue) {
			JsonElement jsonElement = jsonObject.get(jsonElementName);
			return (jsonElement instanceof JsonNull) ? defaultValue : jsonElement.getAsBoolean();
		}
		
		private static long jsonItemParser(JsonObject jsonObject, String jsonElementName, long defaultValue) {
			JsonElement jsonElement = jsonObject.get(jsonElementName);
			return (jsonElement instanceof JsonNull) ? defaultValue : jsonElement.getAsLong();
		}
		
		private static int jsonItemParser(JsonObject jsonObject, String jsonElementName, int defaultValue) {
			JsonElement jsonElement = jsonObject.get(jsonElementName);
			return (jsonElement instanceof JsonNull) ? defaultValue : jsonElement.getAsInt();
		}
		
		private static ZonedDateTime jsonItemParser(JsonObject jsonObject, String jsonElementName, ZonedDateTime defaultValue) {
			JsonElement jsonElement = jsonObject.get(jsonElementName);
			return (jsonElement instanceof JsonNull) ? defaultValue : ZonedDateTime.parse(jsonElement.getAsString());
		}
		
		private static URI jsonItemParser(JsonObject jsonObject, String jsonElementName, URI defaultValue) {
			JsonElement jsonElement = jsonObject.get(jsonElementName);
			return (jsonElement instanceof JsonNull) ? defaultValue : URI.create(jsonElement.getAsString());
		}
		
		private static boolean jsonElementIsNull(JsonObject jsonObject, String jsonElementName) {
			JsonElement jsonElement = jsonObject.get(jsonElementName);
			return (jsonElement instanceof JsonNull);
		}
	}
}
