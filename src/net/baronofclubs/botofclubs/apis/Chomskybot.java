package net.baronofclubs.botofclubs.apis;

import com.google.code.chatterbotapi.ChatterBot;
import com.google.code.chatterbotapi.ChatterBotFactory;
import com.google.code.chatterbotapi.ChatterBotSession;
import com.google.code.chatterbotapi.ChatterBotType;
import com.google.code.chatterbotapi.*;

public class Chomskybot {
	public String botResponse(String botType, String prompt) throws Exception {
		ChatterBotFactory factory = new ChatterBotFactory();

        ChatterBot pandora = factory.create(ChatterBotType.PANDORABOTS, "b0dafd24ee35a477");
        ChatterBotSession pandorasession = pandora.createSession();
        String response = "";
        
        try {
			response = pandorasession.think(prompt);
		} catch (Exception e) {
			response = "Uhhh... Me broked.";
		}
        
        return "Chomsky says: " + response;
	}
	
}
